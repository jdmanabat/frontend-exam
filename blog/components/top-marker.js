import React, { useEffect, useContext } from "react";
import { useInView } from "react-intersection-observer";

import SiteContext from "../context/site-context";

function TopMarker() {
  const { ref, inView } = useInView({
    threshold: 0,
  });

  const { setShowScrollTop } = useContext(SiteContext);

  useEffect(() => {
    if (inView) {
      setShowScrollTop(false);
    } else {
      setShowScrollTop(true);
    }
  });

  return <div ref={ref} />;
}

export { TopMarker };
