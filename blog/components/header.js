import React from "react";
import Link from "next/link";

import { ControllerIcon } from "./vectors";

function Header() {
  return (
    <nav className="sticky top-0 z-20 bg-white shadow">
      <div className="container mx-auto flex justify-between px-4 py-2">
        <Link href="/">
          <a className="text-3xl font-bold uppercase flex items-center text-gray">
            <ControllerIcon className="h-10 mr-1 text-gray" /> Blog
          </a>
        </Link>

        <Link href="/login">
          <a className="text-xl font-bold uppercase flex items-center">Login</a>
        </Link>
      </div>
    </nav>
  );
}

export { Header };
