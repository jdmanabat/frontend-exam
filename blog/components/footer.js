import React from "react";
import Link from "next/link";

import { ControllerIcon } from "./vectors";

function Footer() {
  return (
    <footer>
      <div className="bg-gray px-4 py-2 text-white">
        <div className="container mx-auto">
          <a
            href="#topMarker"
            className="text-3xl font-bold uppercase flex items-center text-white mb-4"
          >
            <ControllerIcon className="h-10 mr-1 text-white" /> Blog
          </a>

          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
          <p>Lorem ipsum dolor sit amet.</p>
        </div>
      </div>
      <div className="bg-black px-4 py-2 text-white">
        <p className="text-center">Copyright&#169;2017-2019 Blog Inc.</p>
      </div>
    </footer>
  );
}

export { Footer };
