import React from "react";

import { ChevronUpIcon } from "./vectors";

function ScrollTop() {
  return (
    <a href="#topMarker" className="fixed bottom-4 right-4 bg-white p-2 shadow">
      <div className="flex flex-col items-center">
        <ChevronUpIcon className="h-4 text-black" />
        <span className="uppercase text-black text-lg block">Top</span>
      </div>
    </a>
  );
}

export { ScrollTop };
