import React, { useState } from "react";
import { ChevronUpIcon } from "./vectors";
import { motion, AnimatePresence } from "framer-motion";
import moment from "moment";

function Hero({ posts }) {
  const [index, setIndex] = useState(0);

  function slideRight() {
    setIndex((index + 1) % posts.length);
  }

  function slideLeft() {
    let nextIndex = index - 1;
    if (nextIndex < 0) {
      setIndex(posts.length - 1);
    } else {
      setIndex(nextIndex);
    }
  }

  return (
    posts.length > 0 && (
      <div className="relative h-0 aspect-ratio-4/3 lg:aspect-ratio-16/9">
        <AnimatePresence>
          <motion.div
            key={index}
            initial={{ opacity: 0 }}
            animate={{ opacity: 1 }}
            exit={{ opacity: 0 }}
            className="absolute inset-0 flex"
          >
            <div className="flex-1 bg-gray">
              <div className="container mx-auto px-4 py-2 flex items-center justify-end h-full w-full">
                <div className="max-w-xs">
                  <p className="text-4xl text-highlight text-right text-black uppercase">
                    {posts[index].content}
                  </p>
                  <p className="text-white text-xl text-right font-bold">
                    {posts[index].createdAt
                      ? moment(posts[index].createdAt).format("YYYY.MM.YY")
                      : ""}
                  </p>
                </div>
              </div>
            </div>
          </motion.div>
        </AnimatePresence>

        <div className="absolute inset-0 flex items-center justify-between w-full px-4 mx-auto">
          <button
            className="border-none bg-transparent pointer"
            onClick={slideLeft}
          >
            <ChevronUpIcon className="h-10 top-0 bottom-0 text-white -rotate-90 " />
          </button>
          <button
            className="border-none bg-transparent pointer"
            onClick={slideRight}
          >
            <ChevronUpIcon className="h-10 top-0 bottom-0 text-white rotate-90 " />
          </button>
        </div>
        <div className="absolute w-full justify-center flex bottom-4">
          {posts.map((post, i) => {
            return (
              <button
                onClick={() => setIndex(i)}
                key={post.id}
                className={`mr-1 h-4 w-4 rounded-full border-none pointer ${
                  index === i ? "bg-black" : "bg-white"
                }`}
              ></button>
            );
          })}
        </div>
      </div>
    )
  );
}

export { Hero };
