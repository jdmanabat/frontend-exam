import React, { useState, useEffect } from "react";
import moment from "moment";
import Image from "next/image";
import _ from "lodash";
import { motion, AnimatePresence } from "framer-motion";

function News({ posts }) {
  const postChunks = _.chunk(posts, 6);
  const [index, setIndex] = useState(0);
  const [currentPosts, setCurrentPosts] = useState(postChunks[0]);

  useEffect(() => {
    let posts = currentPosts;
    if (index !== 0) {
      setCurrentPosts([...posts, ...postChunks[index]]);
    }
  }, [index]);

  return (
    <div className="container mx-auto px-4 py-4">
      <h2 className="uppercase text-4xl">News</h2>
      <div className=" grid sm:grid-cols-2 md:grid-cols-3 gap-4">
        <AnimatePresence>
          {currentPosts.map((post) => {
            return (
              <motion.a
                initial={{ opacity: 0 }}
                animate={{ opacity: 1 }}
                exit={{ opacity: 0 }}
                key={post.id}
                href={`/post/${post.id}`}
                className="px-2 py-2 border border-black border-solid"
              >
                <div className="relative bg-gray">
                  <div className="aspect-ratio-4/3"></div>
                  {post.image ? (
                    <Image
                      src={post.image}
                      className="absolute left-0 top-0 w-full h-full object-cover"
                    />
                  ) : (
                    ""
                  )}
                </div>
                <p className="text-sm font-bold">
                  {post.createdAt
                    ? moment(post.createdAt).format("YYYY.MM.YY")
                    : ""}
                </p>
                <p>{post.content}</p>
              </motion.a>
            );
          })}
        </AnimatePresence>
      </div>
      {index < postChunks.length - 1 ? (
        <div className="flex justify-center mt-4">
          <button
            onClick={() => setIndex((previousIndex) => previousIndex + 1)}
            className="inline-block bg-black py-2 px-4 text-white uppercase text-2xl font-bold border-none pointer"
          >
            Load more
          </button>
        </div>
      ) : (
        ""
      )}
    </div>
  );
}

export { News };
