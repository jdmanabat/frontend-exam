import React, { useContext } from "react";
import { motion, AnimatePresence } from "framer-motion";

import SiteContext from "../context/site-context";

import { Header } from "./header";
import { Footer } from "./footer";
import { TopMarker } from "./top-marker";
import { ScrollTop } from "./scroll-top";

function Layout({ children }) {
  const { showScrollTop } = useContext(SiteContext);

  return (
    <div className="relative mx-auto flex flex-col min-h-screen">
      <div id="topMarker" />
      <Header />
      <TopMarker />
      <main className="flex-1">{children}</main>
      <Footer />
      <AnimatePresence>
        {showScrollTop ? (
          <motion.div
            initial={{ opacity: 0 }}
            animate={{ opacity: 1 }}
            exit={{ opacity: 0 }}
          >
            <ScrollTop />
          </motion.div>
        ) : (
          ""
        )}
      </AnimatePresence>
    </div>
  );
}

export { Layout };
