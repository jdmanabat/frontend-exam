import React, { useState } from "react";

const SiteContext = React.createContext({ showScrollTop: false });

const SiteProvider = ({ children }) => {
  const [showScrollTop, setShowScrollTop] = useState(false);

  return (
    <SiteContext.Provider
      value={{
        showScrollTop,
        setShowScrollTop,
      }}
    >
      {children}
    </SiteContext.Provider>
  );
};

export default SiteContext;
export { SiteProvider };
