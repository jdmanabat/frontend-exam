import Head from "next/head";
import { ApolloClient, InMemoryCache, gql } from "@apollo/client";

import { Layout, Hero, News } from "../components";

export default function Home({ posts }) {
  return (
    <Layout>
      <Head>
        <title>Home | Blog</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Hero posts={posts} />
      <News posts={posts} />
    </Layout>
  );
}

export async function getStaticProps() {
  const client = new ApolloClient({
    uri: "http://localhost:4000/",
    cache: new InMemoryCache(),
  });

  const { data } = await client.query({
    query: gql`
      query GetPosts {
        posts {
          id
          content
          title
          image
          createdAt
        }
      }
    `,
  });

  return {
    props: {
      posts: data.posts,
    },
  };
}
