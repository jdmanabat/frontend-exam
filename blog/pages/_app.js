import "../styles/globals.scss";
import { SiteProvider } from "../context/site-context";

function MyApp({ Component, pageProps }) {
  return (
    <SiteProvider>
      <Component {...pageProps} />{" "}
    </SiteProvider>
  );
}

export default MyApp;
