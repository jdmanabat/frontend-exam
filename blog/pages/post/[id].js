import Head from "next/head";
import Link from "next/link";
import Image from "next/image";
import moment from "moment";

import { Layout } from "../../components";

import { ApolloClient, InMemoryCache, gql } from "@apollo/client";

export default function PostPage({ props: { post } }) {
  return (
    <Layout>
      <Head>
        <title>{post.title} | Blog</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className="bg-gray-200 py-4">
        <div className="container mx-auto px-4 flex">
          <Link href="/">
            <a className="uppercase inline-block mr-1"> Home</a>
          </Link>
          ><p className="uppercase my-0 ml-1"> {post.title}</p>
        </div>
      </div>
      <div className="container mx-auto px-4 py-4 border-bottom">
        <p className="text-lg">
          {post.createdAt ? moment(post.createdAt).format("YYYY.MM.YY") : ""}
        </p>
        <p className="uppercase text-2xl font-bold"> {post.title}</p>
        <div className="relative bg-gray mb-4">
          <div className="aspect-ratio-8/5"></div>
          {post.image ? (
            <Image
              src={post.image}
              className="absolute left-0 top-0 w-full h-full object-cover"
            />
          ) : (
            ""
          )}
        </div>
        <div>{post.content}</div>
      </div>
      <div className="container mx-auto px-4 py-4">
        <h3 className="font-bold text-4xl uppercase mb-4">Comments</h3>
        {post.comments.map((comment, i) => {
          return (
            <div className="px-4 py-4 mb-4 bg-gray-200">
              <p>{comment.content}</p>
              <span className="font-bold">
                {moment(comment.createdAt).fromNow()}
              </span>
            </div>
          );
        })}
      </div>
    </Layout>
  );
}

PostPage.getInitialProps = async ({ query: { id } }) => {
  const client = new ApolloClient({
    uri: "http://localhost:4000/",
    cache: new InMemoryCache(),
  });

  const { data } = await client.query({
    query: gql`
      query Query($postId: Int) {
        post(id: $postId) {
          title
          content
          image
          createdAt
          comments {
            postId
            content
            createdAt
          }
        }
      }
    `,
    variables: { postId: parseInt(id) },
  });

  return {
    props: {
      post: data.post,
    },
  };
};
